var sourceUrl = "https://gitlab.com/codeandsupply/supplybot";

module.exports = function(robot) {
  robot.hear(/the bot should/i, function(msg) {
    msg.send("I'm open source. Merge requests accepted at " + sourceUrl);
  });
  robot.hear(/where is the code for the bot/i, function(msg) {
    msg.send(sourceUrl);
  });
  robot.respond(/show me the code/i, function(msg) {
    msg.send(sourceUrl);
  });
};
