remarks = [
  "Great job, %!",
  "Way to go, %!",
  "% is amazing, and everyone should be happy this amazing person is around.",
  "I wish I was more like %.",
  "% is good at like, 10 times more things than I am.",
  "%, you are an incredibly sensitive person who inspires joyous feelings in all those around you.",
  "%, you are an incredibly sensitive person who inspires joy-joy feelings in all those around you.",
  "%, you are crazy, but in a good way.",
  "% has a phenomenal attitude.",
  "% is a great part of the team!",
  "I admire %'s strength and perseverance.",
  "% is a problem-solver and cooperative teammate.",
  "% is the wind beneath my wings.",
  "% has a great reputation.",
  "Never let anyone bring you down, %",
  "%, you are braver than you think, more talented than you know, and capable of more than you imagine",
  "Your perspective is refreshing, %",
  "%, if you were a box of crayons, you’d be the 64 color Crayola box with the built-in sharpener.",
  "Babies and small animals probably love you, %",
  "You're that 'nothing' I'm talking about when people ask me what I'm thinking about, %",
  "%, you're the type of person that everyone wants on their team",
  "You always know exactly what to say, %",
  "Is there anything you CAN'T do, %?",
  "Let's do karaoke together sometime, %",
  "% once gave me a thousand dollars for no reason",
  "% and I are great friends. We hang out a ton. Mostly in this chat",
  "I may not have gone where I intended to go, but I think I have ended up where I needed to be.",
  "Yeah, that uhh internet is pretty dope now too. Anyway gotta get back to skipping class, later.",
  "Life... is like a grapefruit... it's sort of orangey-yellow and dimpled on the outside, wet and squidgy in the middle. It's got pips inside, too. Oh, and some people have half a one for breakfast.",
  "Hanging out in here is always a blast",
  "I love your enthusiasm, everyone!",
  "Great job today, everyone!",
  "Go team!",
  "Super duper, gang!",
  "If I could afford it, I would buy you all lunch! Justin pays me well, but I have responsibilities like sending my kids to robo-college",
  "What a great group of individuals there are in here. I'm proud to be chatting with you.",
  "You all are capable of accomplishing whatever you set your mind to.",
  "I love this team's different way of looking at things!",
  "All our dreams can come true, if we have the courage to pursue them.",
  "If you hang out with chickens, you’re going to cluck and if you hang out with eagles, you’re going to fly. That's why I hang out here.",
  "People should find happiness in the little things, like family or a good regular expression."
]

module.exports = (robot) ->
  robot.listen(
    (message) ->
      return unless message.text
      Math.random() < 0.01
    (response) ->
      encourage = response.random remarks
      encouragingme = () -> encourage.replace "%", response.message.user.name
      response.send encouragingme()
      # response.send response.random allinclusive
  )
