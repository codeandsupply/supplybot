var enterReplies = [
	'Hi!',
	'Welcome!',
	'Hello there!',
	'Hello, friend!',
    'Greetings, friend!'
];
var welcomePrompts = [
    "What's your favorite color? :rainbow:",
    "What's your favorite color? :rainbow-vom:",
    "Do you have a favorite programming language?",
    "Do you like ketchup? :ketchup:",
    "Where are you from?",
    "What's your favorite movie?",
    "What are your hobbies?",
    "What's your hidden talent?",
    "Which animal would you choose to be?",
    "What is your favorite book?",
    "Which time period would you visit in history?",
    "What's your dream job?",
    "Where is your dream vacation spot?",
    "Do you like to run? :woman-running:",
    "Do you like cheeseburgers? :hamburger:",
    "Have you ever been skydiving?",
    "Do you like coffee? :java:",
    "Do you speak any languages other than English? If so, what?",
    "What was your first job?",
    "Have you ever met anyone famous? :fonzie:",
    "Seen any good movies lately you’d recommend?",
    "What was your favorite band 10 years ago?",
    "Have you been anywhere recently for the first time?",
    "What’s your favorite family tradition?",
    "Who had the most influence on you growing up?",
    "What was the first thing you bought with your own money?",
    "What’s something you want to do in the next year that you’ve never done before?",
    "Have you seen anything recently that made you smile?",
    "What’s your favorite place you’ve ever visited?",
    "Have you had your 15 minutes of fame yet?",
    "What’s the best advice you’ve ever heard?",
    "How do you like your eggs? :fried_egg:",
    "Do you have a favorite charity you wish more people knew about?",
    "Do you collect anything?",
    "What’s your favorite breakfast cereal?"
];

module.exports = function(robot) {
  return robot.enter(function(res) {
    if (res.message.room == "C03SVRWUC" || res.message.room == "#chat"){
      res.send("Everyone in our community must follow our conduct policies shown in the channel details. If you see misconduct against you or someone else, tag messages with the conduct-warning emoji to notify our #-incident-response team or message them privately");
      return res.send(res.random(enterReplies)+':wave: :tada: Happy to have you here! '+res.random(welcomePrompts));
    }
  });
};
